import { expect } from '@playwright/test';

import { test } from '../../fixtures/test/index.js';

const { BOT_ACCOUNT_EMAIL = 'bot@appsemble.com' } = process.env;

enum EmployeeRole {
  Medewerker,
  Secretariaat,
  Receptionist,
}

test.describe('Bezoekers', () => {
  test.beforeEach(async ({ visitApp }) => {
    await visitApp('bezoekers');
  });

  test('should create a meeting and update visitors status', async ({ context, page }) => {
    const browser = context.browser()?.browserType().name();
    await page
      .getByTestId('app-role')
      .selectOption({ value: EmployeeRole[EmployeeRole.Medewerker] });
    await page.getByTestId('create-account').click();

    await page.getByRole('main').getByRole('link', { name: 'Announce visitor' }).click();

    await page.fill('#visitors\\.0\\.name', `Bot-${browser}-1`);
    await page.fill('#visitors\\.0\\.organization', 'Amersfoort');
    await page.fill('#visitors\\.0\\.email', BOT_ACCOUNT_EMAIL);

    await page.click('span:has-text("Add new visitor")');

    await page.fill('#visitors\\.1\\.name', `Bot-${browser}-2`);
    await page.fill('#visitors\\.1\\.organization', 'Amersfoort');
    await page.fill('#visitors\\.1\\.email', 'visitor@example.com');

    await page.locator('#location').selectOption({ index: 0 });
    await page.click('#datetime');
    await page.click(
      'form > div:nth-child(3) > div:nth-child(3) > div.flatpickr-calendar.hasTime.animate.arrowTop.arrowLeft.open > div.flatpickr-innerContainer > div > div.flatpickr-days > div > span.flatpickr-day.today',
    );
    await page.fill('#notes', 'Notes');
    await page.fill('#visited\\.name', `Bot-${browser}-1`);
    await page.fill('#visited\\.email', BOT_ACCOUNT_EMAIL);
    await page.fill('#visited\\.phone', '+123456789');

    await page.getByRole('button', { name: 'Submit' }).click();

    await expect(
      page
        .getByRole('row')
        .getByRole('gridcell', { name: `Bot-${browser}-1` })
        .first(),
    ).toBeVisible();

    await page.getByAltText('Profile Picture').click();
    await page.getByTestId('change-role').click();

    await page
      .getByTestId('app-role')
      .selectOption({ value: EmployeeRole[EmployeeRole.Receptionist] });
    await page.getByTestId('create-account').click();

    await page.waitForURL('**/visitors');

    const visitor1row = page
      .getByRole('row')
      .filter({ has: page.getByRole('gridcell', { name: `Bot-${browser}-1` }) })
      .first();

    visitor1row.getByRole('button').click();
    visitor1row.getByRole('button', { name: 'Announce' }).click();

    await expect(visitor1row.getByRole('gridcell', { name: '✔️' })).toBeVisible();

    const visitor2row = page
      .getByRole('row')
      .filter({ has: page.getByRole('gridcell', { name: `Bot-${browser}-2` }) })
      .first();

    visitor2row.getByRole('button').click();
    visitor2row.getByRole('button', { name: 'No show' }).click();

    await expect(visitor2row.getByRole('gridcell', { name: '❌' })).toBeVisible();
  });
});
