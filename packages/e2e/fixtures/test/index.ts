import { test as base } from '@playwright/test';

interface Fixtures {
  /**
   * Visit an app.
   *
   * Uses to values from the Appsemble class.
   *
   * @param appPath The path of the app to visit.
   */
  visitApp: (appPath: string) => Promise<void>;
}

const organization = 'amersfoort';

export const test = base.extend<Fixtures>({
  async visitApp({ baseURL, page }, use) {
    await use(async (appPath: string) => {
      await page.goto(
        `http://${appPath}.${organization}.${new URL(baseURL ?? 'http://localhost:9999').host}`,
      );
    });
  },
});
