The visitor app was developed for the municipality of Amersfoort. With this app, employees can
access the planning and manage municipal visits.

## Roles

There are 2 roles: employee and receptionist.

### Employee

Municipality employees can schedule an appointment with one or more visitors. Both the employee
themselves and the visitors will receive an email for confirmation.

Furthermore, employees can view and change their own agreements.

### Receptionist

When a visitor arrives at the location, they must report to reception. The receptionist can keep
track of which visitors have registered via the app. The receptionist can see with whom the visitor
has an appointment and refer them to the correct location within the building. This also means that
it is known at any time which visitors are present in the building.
